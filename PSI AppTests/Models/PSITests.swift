//
//  PSITests.swift
//  PSI AppTests
//
//  Created by Mohit on 15/11/2019.
//  Copyright © 2019 Mohit. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import PSI_App

class PSITests: XCTestCase {
    
    var psiModelTest: PSI!
    
    override func setUp() {
        super.setUp()
        
        let testJSON: JSON = ["region_metadata": NSArray(),
                                        "items": NSArray()]
        
        self.psiModelTest = PSI(json: testJSON)
    }
    
    override func tearDown() {
        self.psiModelTest = nil
        super.tearDown()
    }
    
    func testModelIsNotNil() {
        XCTAssertNotNil(self.psiModelTest)
        XCTAssertNotNil(self.psiModelTest.regionMetadata)
        XCTAssertNotNil(self.psiModelTest.items)
    }
    
}
