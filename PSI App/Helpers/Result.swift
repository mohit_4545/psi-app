//
//  Result.swift
//  PSI App
//
//  Created by Mohit on 14/11/2019.
//  Copyright © 2019 Mohit. All rights reserved.
//

enum Result<D, E> where E: Error {
    case success(payload: D)
    case failure(E?)
}

enum EmptyResult<E> where E: Error {
    case success
    case failure(E?)
}
