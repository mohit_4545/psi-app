//
//  PSI.swift
//  PSI App
//
//  Created by Mohit on 14/11/2019.
//  Copyright © 2019 Mohit. All rights reserved.
//

import Foundation
import SwiftyJSON

class PSI: NSObject {
    var regionMetadata: [JSON]!
    var items: [JSON]!
    
    init(json: JSON) {
        self.regionMetadata = json["region_metadata"].array
        self.items = json["items"].array
    }
}
