//
//  PSIReading.swift
//  PSI App
//
//  Created by Mohit on 14/11/2019.
//  Copyright © 2019 Mohit. All rights reserved.
//

import Foundation

class PSIReading: NSObject {
    var o3SubIndex: Double!
    var pm10TwentyFourHourly: Double!
    var pm10SubIndex: Double!
    var coSubIndex: Double!
    var pm25TwentyFourHourly: Double!
    var so2_sub_index: Double!
    var coEightHourMax: Double!
    var no2OneHourMax: Double!
    var so2TwentyFourHourly: Double!
    var pm25SubIndex: Double!
    var psiTwentyFourHourly: Double!
    var o3EightHourMax: Double!
    var timestamp: String!
}
