//
//  PSIData.swift
//  PSI App
//
//  Created by Mohit on 14/11/2019.
//  Copyright © 2019 Mohit. All rights reserved.
//

import Foundation

class PSIData: NSObject {
    var name: String
    var latitude: Double
    var longitude: Double
    var reading = [PSIReading]()
    
    init(name: String, latitude: Double, longitude: Double) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
}
