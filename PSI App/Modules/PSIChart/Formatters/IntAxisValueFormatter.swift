//
//  IntAxisValueFormatter.swift
//  PSI App
//
//  Created by Mohit on 15/11/2019.
//  Copyright © 2019 Mohit. All rights reserved.
//

import Foundation
import Charts

public class IntAxisValueFormatter: NSObject, IAxisValueFormatter {
    private var arrValue: [String]
    
    public init(value: [String]) {
        self.arrValue = value
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return self.arrValue[Int(value)]
    }
}
