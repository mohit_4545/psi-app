//
//  InfoWindow.swift
//  PSI App
//
//  Created by Mohit on 15/11/2019.
//  Copyright © 2019 Mohit. All rights reserved.
//

import UIKit

class InfoWindow: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
